import 'dart:io';
import 'dart:typed_data';

import 'package:esys_flutter_share/esys_flutter_share.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_native_image/flutter_native_image.dart';

class PreviewScreen extends StatefulWidget {
  final String imgPath;
  final String fileName;

  PreviewScreen({this.imgPath, this.fileName});

  @override
  _PreviewScreenState createState() => _PreviewScreenState();
}

class _PreviewScreenState extends State<PreviewScreen> {
  File imageFile;

  @override
  void initState() {
    super.initState();
  }

  _cropImage(filePath) async {
    ImageProperties properties =
        await FlutterNativeImage.getImageProperties(widget.imgPath);
    File croppedFile = await FlutterNativeImage.compressImage(widget.imgPath,
        quality: 100, targetWidth: 1240, targetHeight: 2204);

    // Image thumbnail = copyResize(image, width: 120);
    //File croppedFile =
    //    await FlutterNativeImage.cropImage(widget.imgPath, 0, 0, 720, 960);

    /*File croppedImage = await ImageCropper.cropImage(
      sourcePath: filePath,
      aspectRatioPresets: [CropAspectRatioPreset.ratio3x2],
      maxWidth: 1240,
      maxHeight: 1844,
    );*/

    if (croppedFile != null) {
      imageFile = croppedFile;
    }

    //setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    //_cropImage(widget.imgPath);

    //_cropImage(null);
    //imageFile = new File(widget.imgPath);

    return Scaffold(
        appBar: AppBar(
          automaticallyImplyLeading: true,
        ),
        body: Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              new AspectRatio(
                aspectRatio: 2 / 3,
                child: Stack(
                  children: <Widget>[
                    Image.file(
                      File(widget.imgPath),
                      fit: BoxFit.fitWidth,
                    ),
                    //Image.asset('assets/images/cadre_portrait.png'),
                  ],
                ),
              ),
              /*Expanded(
                flex: 2,
                child: Image.file(
                  //_cropImage(widget.imgPath),
                  File(widget.imgPath),
                  fit: BoxFit.cover,
                ),
              ),*/
              Align(
                alignment: Alignment.bottomCenter,
                child: Container(
                  width: double.infinity,
                  height: 60,
                  color: Colors.blueGrey,
                  child: Center(
                    child: IconButton(
                      icon: Icon(
                        Icons.print,
                        color: Colors.white,
                      ),
                      onPressed: () {
                        getBytes().then((bytes) {
                          print('here now');
                          print(widget.imgPath);
                          print(bytes.buffer.asUint8List());
                          Share.file('Share via', widget.fileName,
                              bytes.buffer.asUint8List(), 'image/path');
                        });
                      },
                    ),
                  ),
                ),
              )
            ],
          ),
        ));
  }

  Future getBytes() async {
    Uint8List bytes = File(widget.imgPath).readAsBytesSync() as Uint8List;
//    print(ByteData.view(buffer))
    return ByteData.view(bytes.buffer);
  }
}
